{include file="common/subheader.tpl" title=__("rus_hybrid_auth.link_provider")}

<div id="hybrid_providers">
{foreach from=$available_providers item="provider_id"}

    <span class="rus-hybrid-auth-icon{if !in_array($provider_id, $link_providers)} link-unlink-provider{/if}">
        <img src="{$images_dir}/addons/rus_hybrid_auth/icons/flat_32x32/{$provider_id}.png" title="{$provider_id}" />
    </span>

{/foreach}
<!--hybrid_providers--></div>