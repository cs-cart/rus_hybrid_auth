<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;

function fn_hybrid_auth_google_callback_url()
{
    return Registry::get('view')->fetch('addons/hybrid_auth/components/google_callback_url.tpl');
}

function fn_settings_variants_addons_rus_hybrid_auth_icons_pack()
{
    $available_icons_packs = array();

    $theme_name = Settings::instance()->getValue('theme_name', '');
    $icons_dir = fn_get_theme_path('[themes]/', 'C') . $theme_name . '/media/images/addons/rus_hybrid_auth/icons/';

    $icons_packs = fn_get_dir_contents($icons_dir);

    foreach ($icons_packs as $id => $icons_packs_name) {
        $available_icons_packs[$icons_packs_name] = $icons_packs_name;
    }

    return $available_icons_packs;
}

function fn_rus_hybrid_auth_post_delete_user($user_id, $user_data, $result)
{
    return db_query("DELETE FROM ?:rus_hybrid_auth_users WHERE user_id = ?i", $user_id);
}

function fn_rus_hybrid_auth_get_unlink_provider($user_id, $provider)
{
    return db_query("DELETE FROM ?:rus_hybrid_auth_users WHERE user_id = ?i AND provider = ?s", $user_id, $provider);
}

function fn_rus_hybrid_auth_get_link_provider($user_id)
{
    $provider = db_get_fields("SELECT provider FROM ?:rus_hybrid_auth_users WHERE user_id = ?i", $user_id);

    return (!empty($provider)) ? $provider : array();
}

function fn_rus_hybrid_auth_get_available_providers()
{
    $available_providers = array();
    $config = Registry::get('addons.rus_hybrid_auth');

    foreach ($config as $config_name => $config_value) {
        if ($config_value == 'Y' && strpos($config_name, '_status')) {
            $available_providers[] = substr($config_name, 0, strpos($config_name, '_status'));
        }
    }

    return $available_providers;
}

function fn_rus_hybrid_auth_get_all_providers()
{
    $providers = array();
    $config = Registry::get('addons.rus_hybrid_auth');

    foreach ($config as $config_name => $config_value) {
        if (strpos($config_name, '_status')) {
            $name = substr($config_name, 0, strpos($config_name, '_status'));
            $providers[$name] = $name;
        }
    }

    return $providers;
}