<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Mailer;

if (!defined('BOOTSTRAP')) { die('Access denied'); }


if ($mode == 'login_provider') {
    // change the following paths if necessary
    $lib_path = Registry::get('config.dir.addons') . 'rus_hybrid_auth/lib/';
    $config = $lib_path . 'config.php';

    require_once($lib_path . 'Hybrid/Auth.php');

    $view = Registry::get('view');

    try {
        $hybridauth = new Hybrid_Auth($config);
    }

    // if sometin bad happen
    catch( Exception $e ){
        $message = "";

        switch ( $e->getCode() ) {
            case 0 : $message = __('rus_hybrid_auth.unspecified_error'); break;
            case 1 : $message = __('rus_hybrid_auth.configuration_error'); break;
            case 2 : $message = __('rus_hybrid_auth.provider_error_configuration'); break;
            case 3 : $message = __('rus_hybrid_auth.wrong_provider'); break;
            case 4 : $message = __('rus_hybrid_auth.missing_credentials'); break;
            case 5 : $message = __('rus_hybrid_auth.failed_auth'); break;

            default: $message = __('hybrid_auth_unspecified_error');
        }

        fn_set_notification('E', __('error'), $message);
        $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');

        exit();
    }

    $provider  = !empty($_REQUEST['provider']) ? $_REQUEST['provider'] : '';

    if (!empty($provider) && $hybridauth->isConnectedWith($provider)) {
        $adapter = $hybridauth->getAdapter($provider);

        try {
            $auth_data = $adapter->getUserProfile();
        } catch (Exception $e) {
            fn_set_notification('E', __('error'), $e->getMessage());

            $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');
            exit();
        }

        $condition = db_quote('identifier = ?s', $auth_data->identifier);

        if (fn_allowed_for('ULTIMATE')) {
            if (Registry::get('settings.Stores.share_users') == 'N' && AREA != 'A') {
                $condition .= fn_get_company_condition('?:users.company_id');
            }
        }

        $join = 'JOIN ?:rus_hybrid_auth_users ON ?:rus_hybrid_auth_users.user_id = ?:users.user_id';
        $user_data = db_get_row("SELECT ?:users.user_id, password FROM ?:users $join WHERE $condition");

        if (empty($user_data['user_id'])) {
            Registry::get('settings.General.address_position') == 'billing_first' ? $address_zone = 'b' : $address_zone = 's';
            $user_data = array();
            $user_data['email'] = (!empty($auth_data->verifiedEmail)) ? $auth_data->verifiedEmail : ((!empty($auth_data->email)) ? $auth_data->email : '');
            $user_data['user_login'] = (!empty($auth_data->verifiedEmail)) ? $auth_data->verifiedEmail : ((!empty($auth_data->email)) ? $auth_data->email : $auth_data->displayName);
            $user_data['user_type'] = 'C';
            $user_data['is_root'] = 'N';
            $user_data['password'] = $user_data['password1'] = $user_data['password2'] = fn_generate_password();
            $user_data[$address_zone . '_firstname'] = (!empty($auth_data->firstName)) ? $auth_data->firstName : '';
            $user_data[$address_zone . '_lastname'] = (!empty($auth_data->lastName)) ? $auth_data->lastName : '';

            if (empty($user_data['email'])) {
                $user_data['email'] = 'noemail-' . TIME . '@example.com';
            }

            $user_id = fn_is_user_exists(0, array('email' => $user_data['email']));

            if (empty($user_id)) {
                list($user_data['user_id'], $profile_id) = fn_update_user('', $user_data, $auth, true, false, false);

                $_user_data = array(
                    'user_id' => $user_data['user_id'],
                    'provider' => $provider,
                    'identifier' => $auth_data->identifier,
                    'timestamp' => TIME,
                );

                db_query("REPLACE INTO ?:rus_hybrid_auth_users ?e", $_user_data);

                if (!empty($user_data['email'])) {
                    $result = Mailer::sendMail(array(
                        'to' => $user_data['email'],
                        'from' => 'company_orders_department',
                        'data' => array(
                            'user_data' => $user_data,
                            'user_name' => $user_data[$address_zone . '_firstname'] . " " . $user_data[$address_zone . '_lastname'],
                        ),
                        'tpl' => 'addons/rus_hybrid_auth/create_profile.tpl',
                    ), 'C', DESCR_SL);
                }

            } else {
                $user_status = LOGIN_STATUS_USER_EXIST;
            }
        }

        if (empty($user_status) || $user_status != LOGIN_STATUS_USER_EXIST) {
            $user_status = (empty($user_data['user_id'])) ? LOGIN_STATUS_USER_NOT_FOUND : fn_login_user($user_data['user_id']);
        }

        $redirect_url = (!empty($_REQUEST['redirect_url'])) ? $_REQUEST['redirect_url'] : fn_url();

        if ($user_status == LOGIN_STATUS_OK) {
            if (empty($user_data['password'])) {
                fn_set_notification('W', __('warning'), __('rus_hybrid_auth.need_update_profile'));
                $redirect_url = 'profiles.update';
            }

        } elseif ($user_status == LOGIN_STATUS_USER_DISABLED) {
            fn_set_notification('E', __('error'), __('error_account_disabled'));

        } elseif ($user_status == LOGIN_STATUS_USER_NOT_FOUND) {
            fn_delete_notification('user_exist');
            fn_set_notification('W', __('warning'), __('rus_hybrid_auth.cant_create_profile'));

        } elseif ($user_status == LOGIN_STATUS_USER_EXIST) {

            $_SESSION['rus_hybrid_auth']['email'] = $user_data['email'];
            $_SESSION['rus_hybrid_auth']['identifier'] = $auth_data->identifier;
            $_SESSION['rus_hybrid_auth']['provider'] = $provider;
            $_SESSION['rus_hybrid_auth']['redirect_url'] = $redirect_url;

            $redirect_url = 'auth.connect_social';
        }

        Registry::get('view')->assign('redirect_url', fn_url($redirect_url));
        Registry::get('view')->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');

        exit;
    }

    if (!empty($provider)) {
        $params = array();

        if ($provider == "OpenID") {
            $params["openid_identifier"] = @ $_REQUEST["openid_identifier"];
        }
    }

    if (!empty($_REQUEST['redirect_to_idp'])) {
        try {
            $adapter = $hybridauth->authenticate($provider, $params);

        } catch (Exception $e) {
            fn_set_notification('E', __('error'), $e->getMessage());
            $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');

            exit();
        }

    } else {
        $view->assign('provider', $provider);
        $view->display('addons/rus_hybrid_auth/views/auth/loading.tpl');
    }

    exit;

} elseif ($mode == 'link_provider') {

    // change the following paths if necessary
    $lib_path = Registry::get('config.dir.addons') . 'rus_hybrid_auth/lib/';
    $config = $lib_path . 'config.php';

    require_once($lib_path . 'Hybrid/Auth.php');

    $view = Registry::get('view');

    try {
        $hybridauth = new Hybrid_Auth($config);
    }

        // if sometin bad happen
    catch( Exception $e ){
        $message = "";

        switch ( $e->getCode() ) {
            case 0 : $message = __('hybrid_auth_unspecified_error'); break;
            case 1 : $message = __('hybrid_auth_configuration_error'); break;
            case 2 : $message = __('hybrid_auth_provider_error_configuration'); break;
            case 3 : $message = __('hybrid_auth_wrong_provider'); break;
            case 4 : $message = __('hybrid_auth_missing_credentials'); break;
            case 5 : $message = __('hybrid_auth_failed_auth'); break;

            default: $message = __('hybrid_auth_unspecified_error');
        }

        fn_set_notification('E', __('error'), $message);
        $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');

        exit();
    }

    $provider  = !empty($_REQUEST['provider']) ? $_REQUEST['provider'] : '';

    if (!empty($provider) && $hybridauth->isConnectedWith($provider)) {
        $adapter = $hybridauth->getAdapter($provider);

        try {
            $auth_data = $adapter->getUserProfile();
        } catch (Exception $e) {
            fn_set_notification('E', __('error'), $e->getMessage());

            $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');
            exit();
        }

        $condition = db_quote('identifier = ?s', $auth_data->identifier);

        if (fn_allowed_for('ULTIMATE')) {
            if (Registry::get('settings.Stores.share_users') == 'N' && AREA != 'A') {
                $condition .= fn_get_company_condition('?:users.company_id');
            }
        }

        $join = 'JOIN ?:rus_hybrid_auth_users ON ?:rus_hybrid_auth_users.user_id = ?:users.user_id';
        $user_data = db_get_row("SELECT ?:users.user_id, password FROM ?:users $join WHERE $condition");

        if (empty($user_data['user_id'])) {
            $_user_data = array(
                'user_id' => $user_data['user_id'],
                'provider' => $provider,
                'identifier' => $auth_data->identifier,
                'timestamp' => TIME,
            );

            db_query("REPLACE INTO ?:rus_hybrid_auth_users ?e", $_user_data);
        }

        $user_status = (empty($user_data['user_id'])) ? LOGIN_STATUS_USER_NOT_FOUND : fn_login_user($user_data['user_id']);

        $redirect_url = (!empty($_REQUEST['redirect_url'])) ? $_REQUEST['redirect_url'] : fn_url();

        if ($user_status == LOGIN_STATUS_OK) {
            if (empty($user_data['password'])) {
                fn_set_notification('W', __('warning'), __('hybrid_auth_need_update_profile'));
                $redirect_url = 'profiles.update';
            }

        } elseif ($user_status == LOGIN_STATUS_USER_DISABLED) {
            fn_set_notification('E', __('error'), __('error_account_disabled'));

        } elseif ($user_status == LOGIN_STATUS_USER_NOT_FOUND) {
            fn_delete_notification('user_exist');
            fn_set_notification('W', __('warning'), __('rus_hybrid_auth.cant_create_profile'));
        }

        Registry::get('view')->assign('redirect_url', fn_url($redirect_url));
        Registry::get('view')->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');
        exit;
    }

    if (!empty($provider)) {
        $params = array();

        if ($provider == "OpenID") {
            $params["openid_identifier"] = @ $_REQUEST["openid_identifier"];
        }
    }

    if (!empty($_REQUEST['redirect_to_idp'])) {
        try {
            $adapter = $hybridauth->authenticate($provider, $params);

        } catch (Exception $e) {
            fn_set_notification('E', __('error'), $e->getMessage());
            $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');

            exit();
        }

    } else {
        $view->assign('provider', $provider);
        $view->display('addons/rus_hybrid_auth/views/auth/loading.tpl');
    }

    exit;

} elseif ($mode == 'process') {
    $lib_path = Registry::get('config.dir.addons') . 'rus_hybrid_auth/lib/';

    require_once($lib_path . 'Hybrid/Auth.php');
    require_once($lib_path . 'Hybrid/Endpoint.php');

    Hybrid_Endpoint::process();

} elseif ($mode == 'logout') {
    // Remove Hybrid auth data
    unset($_SESSION['HA::CONFIG'], $_SESSION['HA::STORE']);

} elseif ($mode == 'connect_social') {

    $email = !empty($_SESSION['rus_hybrid_auth']['email']) ? $_SESSION['rus_hybrid_auth']['email'] : '';
    $identifier = !empty($_SESSION['rus_hybrid_auth']['identifier']) ? $_SESSION['rus_hybrid_auth']['identifier'] : '';
    $provider = !empty($_SESSION['rus_hybrid_auth']['provider']) ? $_SESSION['rus_hybrid_auth']['provider'] : '';

    if (!empty($_SESSION['auth']['user_id'])) {
        $_user_data = array(
            'user_id' => $_SESSION['auth']['user_id'],
            'provider' => $provider,
            'identifier' => $identifier,
            'timestamp' => TIME,
        );

        db_query("REPLACE INTO ?:rus_hybrid_auth_users ?e", $_user_data);

        $redirect_url = $_SESSION['rus_hybrid_auth']['redirect_url'];
        unset($_SESSION['rus_hybrid_auth']);

        return array(CONTROLLER_STATUS_REDIRECT, $redirect_url);
    }

    if (AREA != 'A') {
        fn_add_breadcrumb(__('rus_hybrid_auth.connect_social'));
    }

    $user_id = fn_is_user_exists(0, array('email' => $email));

    if (!empty($user_id)) {
        $user_data = fn_get_user_short_info($user_id);
        $user_login = $user_data['user_login'];
    } else {
        $user_login = '';
    }

    Registry::get('view')->assign('user_login', $user_login);
    Registry::get('view')->assign('identifier', $identifier);
    Registry::get('view')->assign('view_mode', 'simple');
}
