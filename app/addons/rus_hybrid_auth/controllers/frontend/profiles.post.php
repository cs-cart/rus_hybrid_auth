<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

// rus_hybrid_auth

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }


if ($mode == 'update') {
    $available_providers = fn_rus_hybrid_auth_get_available_providers();

    Registry::get('view')->assign('available_providers', $available_providers);

    if (!empty($auth['user_id'])) {
        $link_providers = fn_rus_hybrid_auth_get_link_provider($auth['user_id']);
        Registry::get('view')->assign('link_providers', $link_providers);
    }

} elseif ($mode == 'unlink_provider') {

    if (defined('AJAX_REQUEST')) {
        if (!empty($auth['user_id']) && !empty($_REQUEST['provider'])) {
            fn_rus_hybrid_auth_get_unlink_provider($auth['user_id'], $_REQUEST['provider']);
        }

        $available_providers = fn_rus_hybrid_auth_get_available_providers();

        if (!empty($auth['user_id'])) {
            $link_providers = fn_rus_hybrid_auth_get_link_provider($auth['user_id']);
            Registry::get('view')->assign('link_providers', $link_providers);
        }

        Registry::get('view')->assign('available_providers', $available_providers);
        Registry::get('view')->display('views/profiles/update.tpl');

    }

    exit;

} elseif ($mode == 'link_provider') {

    // change the following paths if necessary
    $lib_path = Registry::get('config.dir.addons') . 'rus_hybrid_auth/lib/';
    $config = $lib_path . 'config.php';

    require_once($lib_path . 'Hybrid/Auth.php');

    $view = Registry::get('view');

    try {
        $hybridauth = new Hybrid_Auth($config);
    }

        // if sometin bad happen
    catch( Exception $e ){
        $message = "";

        switch ( $e->getCode() ) {
            case 0 : $message = __('hybrid_auth_unspecified_error'); break;
            case 1 : $message = __('hybrid_auth_configuration_error'); break;
            case 2 : $message = __('hybrid_auth_provider_error_configuration'); break;
            case 3 : $message = __('hybrid_auth_wrong_provider'); break;
            case 4 : $message = __('hybrid_auth_missing_credentials'); break;
            case 5 : $message = __('hybrid_auth_failed_auth'); break;

            default: $message = __('hybrid_auth_unspecified_error');
        }

        fn_set_notification('E', __('error'), $message);
        $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');

        exit();
    }

    $provider  = !empty($_REQUEST['provider']) ? $_REQUEST['provider'] : '';

    if (!empty($provider) && $hybridauth->isConnectedWith($provider)) {
        $adapter = $hybridauth->getAdapter($provider);

        try {
            $auth_data = $adapter->getUserProfile();
        } catch (Exception $e) {
            fn_set_notification('E', __('error'), $e->getMessage());

            $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');
            exit();
        }

        $condition = db_quote('identifier = ?s', $auth_data->identifier);

        if (fn_allowed_for('ULTIMATE')) {
            if (Registry::get('settings.Stores.share_users') == 'N' && AREA != 'A') {
                $condition .= fn_get_company_condition('?:users.company_id');
            }
        }

        $join = 'JOIN ?:rus_hybrid_auth_users ON ?:rus_hybrid_auth_users.user_id = ?:users.user_id';
        $user_data = db_get_row("SELECT ?:users.user_id, password FROM ?:users $join WHERE $condition");

        if (empty($user_data['user_id'])) {
            $_user_data = array(
                'user_id' => $auth['user_id'],
                'provider' => $provider,
                'identifier' => $auth_data->identifier,
                'timestamp' => TIME,
            );

            db_query("REPLACE INTO ?:rus_hybrid_auth_users ?e", $_user_data);
        } else {
            fn_set_notification('E', __('error'), __("text_rus_hybrid_auth.user_is_already_link"));
        }

        $redirect_url = (!empty($_REQUEST['return_url'])) ? $_REQUEST['return_url'] : fn_url('profiles.update');

        Registry::get('view')->assign('redirect_url', fn_url($redirect_url));
        Registry::get('view')->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');
        exit;
    }

    if (!empty($provider)) {
        $params = array();

        if ($provider == "OpenID") {
            $params["openid_identifier"] = @ $_REQUEST["openid_identifier"];
        }
    }

    if (!empty($_REQUEST['redirect_to_idp'])) {
        try {
            $adapter = $hybridauth->authenticate($provider, $params);

        } catch (Exception $e) {
            fn_set_notification('E', __('error'), $e->getMessage());
            $view->display('addons/rus_hybrid_auth/views/auth/login_error.tpl');

            exit();
        }

    } else {
        $view->assign('provider', $provider);
        $view->display('addons/rus_hybrid_auth/views/auth/loading.tpl');
    }

    exit;

}