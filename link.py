#!/usr/bin/env python

import os, pickle

path_config = os.getenv('HOME', '.') + '/rus_hybrid_auth.simtechdev'
path_addons = os.path.abspath(os.path.dirname(__file__))

path_addons_links = [
    "app/addons",
    "design/backend/media/images/addons",
    "design/backend/templates/addons",
    "design/backend/css/addons",
    "design/themes/basic/templates/addons",
    "design/themes/basic/mail/templates/addons",
    "design/themes/basic/css/addons",
    "design/themes/basic/media/images/addons",
    "design/themes/responsive/templates/addons",
    "design/themes/responsive/mail/templates/addons",
    "design/themes/responsive/css/addons",
    "design/themes/responsive/media/images/addons",
    "var/themes_repository/basic/templates/addons",
    "var/themes_repository/basic/mail/templates/addons",
    "var/themes_repository/basic/css/addons",
    "var/themes_repository/basic/media/images/addons",
    "var/themes_repository/responsive/templates/addons",
    "var/themes_repository/responsive/mail/templates/addons",
    "var/themes_repository/responsive/css/addons",
    "var/themes_repository/responsive/media/images/addons",
    "js/addons" 
]

def read_dict(path):
   dic={}
   for line in open(path):
       key,value=line.strip().split(':')
       dic[key]=value  #dic[key]=int(value)
   return dic


def write_dict(path,dic):
   try:
       f = open(path,"w")
   except IOError:
       print('cannot open', path)

   for key in dic:
       s= "{0}:{1}\n".format(key,dic[key],)

       f.write(s)
   f.close()


def create_link(src, dst):
    if os.path.lexists(dst):
        os.remove(dst)
        print 'Update link ' + dst
    else:
        print 'Create link ' + dst

    os.symlink(src, dst)   


if os.path.exists(path_config):
    config = read_dict(path_config)
else:
    config = {'path_cscart': path_addons}

print "Enter path: [" + config['path_cscart'] + "]"
path_cscart = raw_input()

if path_cscart:
    config['path_cscart'] = path_cscart
else:
    path_cscart = config['path_cscart']

for path_addon_link in path_addons_links:
    src = os.path.join(path_addons, path_addon_link, "rus_hybrid_auth")
    dst = os.path.join(path_cscart, path_addon_link, "rus_hybrid_auth")

    if os.path.exists(src):
        create_link(src, dst)
    
write_dict(path_config, config)
