{include file="common/subheader.tpl" title=__("rus_hybrid_auth.link_provider")}
<p>{__("text_rus_hybrid_auth.link_provider")}</p>

<div id="rus_hybrid_providers">
    {foreach from=$available_providers item="provider_id"}
    <a class="{if in_array($provider_id, $link_providers)}cm-unlink-provider {else}cm-link-provider ty-link-unlink-provider {/if}ty-rus-hybrid-auth-icon" data-idp="{$provider_id}">
        <img src="{$images_dir}/addons/rus_hybrid_auth/icons/{$addons.rus_hybrid_auth.icons_pack}/{$provider_id}.png" title="{$provider_id}"/>
    </a>
    {/foreach}
<!--rus_hybrid_providers--></div>