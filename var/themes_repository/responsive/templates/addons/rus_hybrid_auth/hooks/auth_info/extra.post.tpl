{if $runtime.controller == "auth" && $runtime.mode == "connect_social"}
    <h4 class="ty-login-info__title">{__("text_rus_hybrid_auth.connect_social_title")}</h4>
    <div class="ty-login-info__txt">{__("text_rus_hybrid_auth.connect_social")}</div>
{/if}
