{capture name="rus_hybrid_auth"}

    {strip}
        {foreach $addons.rus_hybrid_auth key="option_name" item="value"}
            {if $option_name|strpos:"_status" !== false && $value == "Y"}
                {$provider_id = "_status"|str_replace:"":$option_name}
                <input type="hidden" name="redirect_url" id="redirect_url" value="{$config.current_url}" />
                <a class="cm-login-provider rus-hybrid-auth-icon" data-idp="{$provider_id}"><img src="{$images_dir}/addons/rus_hybrid_auth/icons/{$addons.rus_hybrid_auth.icons_pack}/{$provider_id}.png" title="{$provider_id}" /></a>
            {/if}
        {/foreach}
    {/strip}
{/capture}

{if $smarty.capture.rus_hybrid_auth}
    {__("rus_hybrid_auth.social_login")}:

    <p class="text-center">{$smarty.capture.rus_hybrid_auth nofilter}</p>
{/if}